Домашнее задание `Prometheus. Grafana`
- выполнить в корневой директории ```helm install otus-hmwrk-2 ./otus-hmwrk-2-chart```


Для нагрузки можно выполнит скрипт ```create_load.sh```, скрипт в беконечном потоке будет запускать postman тест(для построения графиков запускал в 4 потока). 

Конфиги дашбордов лежат в папке [dashboards](dashboards)
Скриншоты дашбордов лежат в папке [screenshots](screenshots)

pods_dashboard взят из самого прометеуса, над доп баллы не претендую.
