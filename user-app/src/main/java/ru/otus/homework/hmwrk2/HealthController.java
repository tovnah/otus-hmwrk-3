package ru.otus.homework.hmwrk2;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {
    @GetMapping(value = "health")
    public Health getHealth() {
        return new Health("OK");
    }

    @GetMapping(value = "error")
    public void error() {
       throw new RuntimeException("Exceptions");
    }

    @Data
    @AllArgsConstructor
    private class Health {
        private String status;
    }
}
